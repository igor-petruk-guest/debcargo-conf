Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gvdb-macros
Upstream-Contact: Fina Wilke <code@felinira.net>
Source: https://github.com/felinira/gvdb-rs

Files: *
Copyright: 2022-2023 Fina Wilke <code@felinira.net>
License: MIT

Files: test-data/gresource/icons/scalable/actions/*.svg
Copyright: GNOME Design Team
License: CC0-1.0
Comment: Taken from https://gitlab.gnome.org/Teams/Design/icon-development-kit

Files: debian/*
Copyright:
 2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2023 Matthias Geiger <werdahias@riseup.net>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all
 copyright and related and neighboring rights to this software to the public
 domain worldwide. This software is distributed without any warranty.
 .
 On Debian systems, the complete text of the CC0 license, version 1.0,
 can be found in /usr/share/common-licenses/CC0-1.0.
