diff --git a/Cargo.toml b/Cargo.toml
index bd6378f..483bedd 100644
--- a/Cargo.toml
+++ b/Cargo.toml
@@ -33,17 +33,17 @@ license = "MIT OR Apache-2.0"
 repository = "https://github.com/rust-lang/packed_simd"
 
 [dependencies.cfg-if]
-version = "1.0.0"
+version = "1"
 
 [dependencies.libm]
-version = "0.1.4"
+version = "0.2"
 
 [dev-dependencies.arrayvec]
-version = "^0.5"
+version = "0.7"
 default-features = false
 
 [dev-dependencies.paste]
-version = "^0.1.3"
+version = "1"
 
 [features]
 default = []
@@ -51,10 +51,7 @@ into_bits = []
 libcore_neon = []
 
 [target.wasm32-unknown-unknown.dev-dependencies.wasm-bindgen]
-version = "=0.2.73"
-
-[target.wasm32-unknown-unknown.dev-dependencies.wasm-bindgen-test]
-version = "=0.3.23"
+version = "0.2"
 
 [badges.appveyor]
 repository = "rust-lang/packed_simd"
diff --git a/src/api/bit_manip.rs b/src/api/bit_manip.rs
index 6d88657..c1e90bb 100644
--- a/src/api/bit_manip.rs
+++ b/src/api/bit_manip.rs
@@ -34,7 +34,7 @@ macro_rules! impl_bit_manip {
 
         test_if! {
             $test_tt:
-            paste::item_with_macros! {
+            paste::item! {
                 #[allow(overflowing_literals)]
                 pub mod [<$id _bit_manip>] {
                     #![allow(const_item_mutation)]
diff --git a/src/api/fmt/binary.rs b/src/api/fmt/binary.rs
index 91c0825..a933ab7 100644
--- a/src/api/fmt/binary.rs
+++ b/src/api/fmt/binary.rs
@@ -24,7 +24,7 @@ macro_rules! impl_fmt_binary {
                     #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn binary() {
                         use arrayvec::{ArrayString,ArrayVec};
-                        type TinyString = ArrayString<[u8; 512]>;
+                        type TinyString = ArrayString<512>;
 
                         use crate::fmt::Write;
                         let v = $id::splat($elem_ty::default());
@@ -35,7 +35,7 @@ macro_rules! impl_fmt_binary {
                         write!(&mut beg, "{}(", stringify!($id)).unwrap();
                         assert!(s.starts_with(beg.as_str()));
                         assert!(s.ends_with(")"));
-                        let s: ArrayVec<[TinyString; 64]>
+                        let s: ArrayVec<TinyString, 64>
                             = s.replace(beg.as_str(), "")
                             .replace(")", "").split(",")
                             .map(|v| TinyString::from(v.trim()).unwrap())
diff --git a/src/api/fmt/debug.rs b/src/api/fmt/debug.rs
index 1e209b3..7f982ba 100644
--- a/src/api/fmt/debug.rs
+++ b/src/api/fmt/debug.rs
@@ -11,7 +11,7 @@ macro_rules! impl_fmt_debug_tests {
                     #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn debug() {
                         use arrayvec::{ArrayString,ArrayVec};
-                        type TinyString = ArrayString<[u8; 512]>;
+                        type TinyString = ArrayString<512>;
 
                         use crate::fmt::Write;
                         let v = $id::default();
@@ -22,7 +22,7 @@ macro_rules! impl_fmt_debug_tests {
                         write!(&mut beg, "{}(", stringify!($id)).unwrap();
                         assert!(s.starts_with(beg.as_str()));
                         assert!(s.ends_with(")"));
-                        let s: ArrayVec<[TinyString; 64]>
+                        let s: ArrayVec<TinyString, 64>
                             = s.replace(beg.as_str(), "")
                             .replace(")", "").split(",")
                             .map(|v| TinyString::from(v.trim()).unwrap())
diff --git a/src/api/fmt/lower_hex.rs b/src/api/fmt/lower_hex.rs
index 8f11d31..cf1b3eb 100644
--- a/src/api/fmt/lower_hex.rs
+++ b/src/api/fmt/lower_hex.rs
@@ -24,7 +24,7 @@ macro_rules! impl_fmt_lower_hex {
                     #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn lower_hex() {
                         use arrayvec::{ArrayString,ArrayVec};
-                        type TinyString = ArrayString<[u8; 512]>;
+                        type TinyString = ArrayString<512>;
 
                         use crate::fmt::Write;
                         let v = $id::splat($elem_ty::default());
@@ -35,7 +35,7 @@ macro_rules! impl_fmt_lower_hex {
                         write!(&mut beg, "{}(", stringify!($id)).unwrap();
                         assert!(s.starts_with(beg.as_str()));
                         assert!(s.ends_with(")"));
-                        let s: ArrayVec<[TinyString; 64]>
+                        let s: ArrayVec<TinyString, 64>
                             = s.replace(beg.as_str(), "").replace(")", "")
                             .split(",")
                             .map(|v| TinyString::from(v.trim()).unwrap())
diff --git a/src/api/fmt/octal.rs b/src/api/fmt/octal.rs
index e708e09..c728813 100644
--- a/src/api/fmt/octal.rs
+++ b/src/api/fmt/octal.rs
@@ -24,7 +24,7 @@ macro_rules! impl_fmt_octal {
                     #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn octal_hex() {
                         use arrayvec::{ArrayString,ArrayVec};
-                        type TinyString = ArrayString<[u8; 512]>;
+                        type TinyString = ArrayString<512>;
 
                         use crate::fmt::Write;
                         let v = $id::splat($elem_ty::default());
@@ -35,7 +35,7 @@ macro_rules! impl_fmt_octal {
                         write!(&mut beg, "{}(", stringify!($id)).unwrap();
                         assert!(s.starts_with(beg.as_str()));
                         assert!(s.ends_with(")"));
-                        let s: ArrayVec<[TinyString; 64]>
+                        let s: ArrayVec<TinyString, 64>
                             = s.replace(beg.as_str(), "").replace(")", "")
                             .split(",")
                             .map(|v| TinyString::from(v.trim()).unwrap())
diff --git a/src/api/fmt/upper_hex.rs b/src/api/fmt/upper_hex.rs
index 5ad4557..a916901 100644
--- a/src/api/fmt/upper_hex.rs
+++ b/src/api/fmt/upper_hex.rs
@@ -24,7 +24,7 @@ macro_rules! impl_fmt_upper_hex {
                     #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn upper_hex() {
                         use arrayvec::{ArrayString,ArrayVec};
-                        type TinyString = ArrayString<[u8; 512]>;
+                        type TinyString = ArrayString<512>;
 
                         use crate::fmt::Write;
                         let v = $id::splat($elem_ty::default());
@@ -35,7 +35,7 @@ macro_rules! impl_fmt_upper_hex {
                         write!(&mut beg, "{}(", stringify!($id)).unwrap();
                         assert!(s.starts_with(beg.as_str()));
                         assert!(s.ends_with(")"));
-                        let s: ArrayVec<[TinyString; 64]>
+                        let s: ArrayVec<TinyString, 64>
                             = s.replace(beg.as_str(), "").replace(")", "")
                             .split(",")
                             .map(|v| TinyString::from(v.trim()).unwrap())
diff --git a/src/api/minimal/ptr.rs b/src/api/minimal/ptr.rs
index c3d61fb..38ab0ae 100644
--- a/src/api/minimal/ptr.rs
+++ b/src/api/minimal/ptr.rs
@@ -236,7 +236,7 @@ macro_rules! impl_minimal_p {
                     #[cfg_attr(target_arch = "wasm32", wasm_bindgen_test)]
                     fn debug() {
                         use arrayvec::{ArrayString,ArrayVec};
-                        type TinyString = ArrayString<[u8; 512]>;
+                        type TinyString = ArrayString<512>;
 
                         use crate::fmt::Write;
                         let v = $id::<i32>::default();
@@ -250,7 +250,7 @@ macro_rules! impl_minimal_p {
                             "s = {} (should start with = {})", s, beg
                         );
                         assert!(s.ends_with(")"));
-                        let s: ArrayVec<[TinyString; 64]>
+                        let s: ArrayVec<TinyString, 64>
                             = s.replace(beg.as_str(), "")
                             .replace(")", "").split(",")
                             .map(|v| TinyString::from(v.trim()).unwrap())
diff --git a/src/api/swap_bytes.rs b/src/api/swap_bytes.rs
index 53bba25..4649ed6 100644
--- a/src/api/swap_bytes.rs
+++ b/src/api/swap_bytes.rs
@@ -76,7 +76,7 @@ macro_rules! impl_swap_bytes {
 
         test_if! {
             $test_tt:
-            paste::item_with_macros! {
+            paste::item! {
                 pub mod [<$id _swap_bytes>] {
                     use super::*;
 
diff --git a/src/codegen/math/float/tanh.rs b/src/codegen/math/float/tanh.rs
index 2c0dd3d..630e2b6 100644
--- a/src/codegen/math/float/tanh.rs
+++ b/src/codegen/math/float/tanh.rs
@@ -15,18 +15,18 @@ macro_rules! define_tanh {
             use core::intrinsics::transmute;
             let mut buf: [$basetype; $lanes] = unsafe { transmute(x) };
             for elem in &mut buf {
-                *elem = <$basetype as $trait>::tanh(*elem);
+                *elem = $trait(*elem);
             }
             unsafe { transmute(buf) }
         }
     };
 
     (f32 => $name:ident, $type:ty, $lanes:expr) => {
-        define_tanh!($name, f32, $type, $lanes, libm::F32Ext);
+        define_tanh!($name, f32, $type, $lanes, libm::tanhf);
     };
 
     (f64 => $name:ident, $type:ty, $lanes:expr) => {
-        define_tanh!($name, f64, $type, $lanes, libm::F64Ext);
+        define_tanh!($name, f64, $type, $lanes, libm::tanh);
     };
 }
 
@@ -43,11 +43,11 @@ define_tanh!(f64 => tanh_v4f64, f64x4, 4);
 define_tanh!(f64 => tanh_v8f64, f64x8, 8);
 
 fn tanh_f32(x: f32) -> f32 {
-    libm::F32Ext::tanh(x)
+    libm::tanhf(x)
 }
 
 fn tanh_f64(x: f64) -> f64 {
-    libm::F64Ext::tanh(x)
+    libm::tanh(x)
 }
 
 gen_unary_impl_table!(Tanh, tanh);
