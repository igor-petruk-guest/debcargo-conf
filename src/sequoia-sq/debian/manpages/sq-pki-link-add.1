.TH SQ 1 0.34.0 "Sequoia PGP" "User Commands"
.SH NAME
sq pki link add \- Link a certificate and a User ID
.SH SYNOPSIS
.br
\fBsq pki link add\fR [\fIOPTIONS\fR] \fIFINGERPRINT|KEYID\fR \fIUSERID|EMAIL\fR
.SH DESCRIPTION
Link a certificate and a User ID.
.PP
This cause `sq` to considers the certificate and User ID binding to be
authentic.
.PP
A certificate can also be accepted as a certification authority, which
is also known as a trusted introducer, by using the `\-\-ca` or
`\-\-depth` option.
.PP
A link can be retracted using `sq pki link retract`.
.PP
This command is similar to `sq pki certify`, but the certifications it
makes are done using the certificate directory's trust root, not an
arbitrary key.  Further, the certificates are marked as
non\-exportable.  The former makes it easier to manage certifications,
especially when the user's certification key is offline.  And the
latter improves the user's privacy, by reducing the chance that parts
of the user's social graph is leaked when a certificate is shared.
.PP
By default a link never expires.
Using the `\-\-expiry` argument specific validity periods may be defined.
It allows for providing a point in time for validity to end or a validity
duration.
.PP
`sq pki link` respects the reference time set by the top\-level `\-\-time`
argument. It sets the link's creation time to the reference time.
.PP


.SH OPTIONS
.SS "Subcommand options"
.TP
\fB\-a\fR, \fB\-\-amount\fR=\fIAMOUNT\fR
Set the amount of trust.  Values between 1 and 120 are meaningful. 120 means fully trusted.  Values less than 120 indicate the degree of trust.  60 is usually used for partially trusted.
.TP
\fB\-\-all\fR
Link all valid self\-signed User ID to the certificate.
.TP
\fB\-\-ca\fR=\fI*|DOMAIN\fR
Mark the certificate as a certification authority for a  domain.  Use `*` to make the certificate a certification
             authority for any User ID.
.IP
             A certification authority is also referred to as a trusted introducer.  This command is equivalent to making the trust depth unconstrained, i.e., setting the depth to 255.  See `\-\-depth` for more information.
.TP
\fB\-d\fR, \fB\-\-depth\fR=\fITRUST_DEPTH\fR
Set the trust depth (sometimes referred to as the trust level).  0 means a normal certification of <CERTIFICATE, USERID>.  1 means CERTIFICATE is also a trusted introducer, 2 means CERTIFICATE is a meta\-trusted introducer, etc.
.TP
\fB\-\-email\fR=\fIEMAIL\fR
An email address to link to the certificate.  The email address must match the email address of a self\-signed User ID.  To link an email address to the certificate that does not appear in a self\-signed User ID, use `\-\-petname`.  If the specified email appears in multiple self\-signed User IDs, then all of them are linked.
.TP
\fB\-\-expiry\fR=\fIEXPIRY\fR
Define EXPIRY for the acceptance as ISO 8601 formatted string or custom duration. If an ISO 8601 formatted string is provided, the validity period reaches from the reference time (may be set using `\-\-time`) to the provided time. Custom durations starting from the reference time may be set using `N[ymwds]`, for N years, months, weeks, days, or seconds. The special keyword `never` sets an unlimited expiry.
.TP
\fB\-\-notation\fR \fINAME\fR \fIVALUE\fR
Add a notation to the certification.  A user\-defined notation's name must be of the form `name@a.domain.you.control.org`. If the notation's name starts with a `!`, then the notation is marked as being critical.  If a consumer of a signature doesn't understand a critical notation, then it will ignore the signature.  The notation is marked as being human readable.
.TP
\fB\-\-petname\fR=\fIPETNAME\fR
A User ID to link to the certificate.  Unlike `\-\-userid`, this does not need to match a self\-signed User ID.  Bare email address are automatically wrapped in angle brackets. That is if `alice@example.org` is provided, it is silently converted to `<alice@example.org>`.
.TP
\fB\-r\fR, \fB\-\-regex\fR=\fIREGEX\fR
Add a regular expression to constrain what a trusted introducer can certify.  The regular expression must match the certified User ID in all intermediate introducers, and the certified certificate. Multiple regular expressions may be specified.  In that case, at least one must match.
.TP
\fB\-\-temporary\fR
Temporarily accepts the binding.  Creates a fully
            trust link between a certificate and one or more
            User IDs for a week.  After that, the link is
            automatically downgraded to a partially trusted link
            (trust = 40).
.TP
\fB\-\-userid\fR=\fIUSERID\fR
A User ID to link to the certificate.  This must match a self\-signed User ID.  To link a User ID to the certificate that does not have a self\-signature, use `\-\-petname`.
.TP
 \fIFINGERPRINT|KEYID\fR
The certificate to accept.
.TP
 \fIUSERID|EMAIL\fR
A User ID or email address to link to the certificate.  This must match a self\-signed User ID.  To link a User ID to the certificate that does not have a self\-signature, use `\-\-petname`.  Scripts should prefer to use `\-\-email` or `\-\-userid`, as `sq` does not need to guess if a value is a User ID or an email address.
.SS "Global options"
See \fBsq\fR(1) for a description of the global options.
.SH EXAMPLES
.PP

.PP
The user links 0123456789ABCDEF and the User ID
\&'<romeo@example.org>'.
.PP
.nf
.RS
sq pki link add 0123456789ABCDEF '<romeo@example.org>'
.RE
.PP
.fi

.PP
The user examines 0123456789ABCDEF and then accepts the certificate
0123456789ABCDEF with its current set of self\-signed User IDs.
.PP
.nf
.RS
sq cert export \-\-cert 0123456789ABCDEF | sq inspect
.RE
.PP
.fi
\&...
.PP
.nf
.RS
sq pki link add 0123456789ABCDEF \-\-all
.RE
.PP
.fi

.PP
The user links the certificate and its current self\-signed User
IDs for a week.
.PP
.nf
.RS
sq pki link add \-\-expires\-in 1w 0123456789ABCDEF \-\-all
.RE
.PP
.fi

.PP
The user accepts the certificate, and its current self\-signed User
IDs as a certification authority.  That is, the certificate is
considered a trust root.
.PP
.nf
.RS
sq pki link add \-\-ca '*' 0123456789ABCDEF \-\-all
.RE
.PP
.fi

.PP
The user accepts the certificate and its current self\-signed User
IDs as a partially trusted certification authority.
.PP
.nf
.RS
sq pki link add \-\-ca '*' \-\-amount 60 0123456789ABCDEF \-\-all
.RE
.PP
.fi

.PP
The user retracts their acceptance of 0123456789ABCDEF and any
associated User IDs.  This effectively invalidates any links.
.PP
.nf
.RS
sq pki link retract 0123456789ABCDEF
.RE
.fi
.SH "SEE ALSO"
.nh
\fBsq\fR(1), \fBsq\-pki\fR(1), \fBsq\-pki\-link\fR(1).
.hy
.PP
For the full documentation see <https://book.sequoia\-pgp.org>.
.SH VERSION
0.34.0 (sequoia\-openpgp 1.19.0)
